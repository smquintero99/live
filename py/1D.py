from pymongo import MongoClient
client = MongoClient('mongodb+srv://SantiagoDZ:darmoslive@dzeeta-l6n4o.mongodb.net/test?retryWrites=true&w=majority')
db = client.live

survey = '5d4edc1d3e0f3cf82b5159d2'
def get_questions(survey):
    return list(db.Questions.find({'survey':survey}))

def get_numeric_questions(survey):
    return list(db.Questions.find({'survey':survey, 'type':'Númerica'}))

def get_questions_ids(survey):
    get_id = lambda doc: str(doc['_id'])
    return list(map(get_id, get_questions(survey)))

#print(get_questions(survey))
#question_ids = get_questions_ids(survey)
def get_answers(question):
    return list(db.Answers.find({'question': question}))

def get_answer_values(question):
    get_value = lambda doc: str(doc['value'])
    return list(map(get_value, get_answers(question)))

def get_numeric_average(l):
    return sum(list(map(lambda n: float(n),l)))/len(l)

#print(get_numeric_average(get_answer_values(question_ids[3])))
def count_answers_pipeline(question_id):
    count_pipeline = [
        {'$match': {'question': question_id}}, 
        {'$count': 'count'}
    ]
    return db.Answers.aggregate(count_pipeline)

def sum_value_pipeline(question_id):
    sum_pipeline = [
        {'$match': {'question': question_id}}, 
        {'$group': {'_id': 'sum', 'count': {'$sum': '$value'}}}
    ]
    return db.Answers.aggregate(sum_pipeline)

def get_average_value(question_id):
    return list(sum_value_pipeline(question_id))[0]['count']/list(count_answers_pipeline(question_id))[0]['count']

# {question:'', average:'', title:''}
def get_all_answer_values(survey):
    questions = get_questions(survey)
    question_ids = list(map(lambda doc: str(doc['_id']), questions))
    question_titles = list(map(lambda doc: doc['title'], questions))
    answer_averages = list(map(lambda id: get_average_value(id), question_ids))
    averages = []
    for i in range(len(questions)):
        averages.append({
            'question_id': question_ids[i], 
            'question_title': question_titles[i], 
            'answer_average': answer_averages[i]
        })
    return averages

def write_averages(survey):
    db.Metrics.insert_many(get_all_answer_values(survey))

def get_primary_metrics(survey):
    metrics_pipeline = [
        { '$match': { 'survey': survey } }, 
        { '$project': {'question': { '$convert': { 'input': '$question', 'to': 'objectId' }}, 'value': '$value'}}, 
        { '$lookup': {'from': 'Questions', 'localField': 'question', 'foreignField': '_id', 'as': 'question' }}, 
        { '$project': {
                'question_id': {'$arrayElemAt': ['$question._id', 0]}, 
                'question_title': {'$arrayElemAt': ['$question.title', 0]}, 
                'value': '$value'
        }}, 
        {'$group': {
            '_id': '$question_id', 
            'count': {'$sum': 1}, 
            'sum': {'$sum': '$value'}, 
            'avg': {'$avg': '$value'}, 
            'std': {'$stdDevPop': '$value'}, 
            'title': {'$max': '$question_title'}
        }}, 
        { '$project': {
            '_id':0, 'question_id': '$_id', 'count': '$count', 'sum': '$sum', 'avg': '$avg', 'std': '$std', 'title': '$title'
        }}
    ]
    return list(db.Answers.aggregate(metrics_pipeline))

def write_metrics(survey): db.Metrics.insert_many(get_primary_metrics(survey))
#write_metrics(survey)

set_interval_length = {'$divide': [{'$add': ['$max', {'$multiply': ['$min', -1]}]}, 20]}
def build_cummulative_intervals(question):
    intervals_pipeline = [
        {'$match': {'question': question}},
        {'$group': {'_id': '$question', 'min': {'$min': '$value'}, 'max': {'$max': '$value'}, 'values': {'$push': '$value'}}}, 
        {'$project': {'results': {'$map': {'input': '$values', 'as': 'value', 'in': {'$multiply': [
            {'$toInt': {'$divide': ['$$value', set_interval_length]}}, {'$toInt': set_interval_length}
        ]}} }}}, 
        {'$unwind': '$results'}, 
        {'$group': { '_id': '$results', 'group': { '$sum': 1 }}},
        {'$project': {'group':'$_id', 'frecuency':'$group', '_id':0}}
    ]
    return list(db.Answers.aggregate(intervals_pipeline))

def group_value_intervals(question):
    value_intervals = [
        {'$match': {'question': question}}, 
        {'$group': {'_id': '$value', 'group': {'$sum': 1}}},
        {'$project': {'group':'$_id', 'frecuency':'$group', '_id':0}}
    ]
    return list(db.Answers.aggregate(value_intervals))

def create_intervals(survey):
    intervals = []
    questions = get_questions(survey)
    question_ids = list(map(lambda doc: str(doc['_id']), questions))
    question_titles = list(map(lambda doc: doc['title'], questions))
    for i, question in enumerate(question_ids):
        interval = {
            'question_id':question, 
            'intervals':group_value_intervals(question), 
            'question_title':question_titles[i]
        }
        intervals.append(interval)
    for interval in intervals:
        if len(interval['intervals']) > 20:
            interval['intervals'] = build_cummulative_intervals(interval['question_id'])
    return intervals

def test_intervals(survey):
    intervals = create_intervals(survey)
    for interval in intervals:
        f = 0
        for i in interval['intervals']:
            f = f + i['frecuency']
    return intervals

def write_intervals(survey):
    intervals = create_intervals(survey)
    for interval in intervals:
        db.Metrics.update_one(
            {'title': interval['question_title']}, 
            {'$set':{'intervals':interval['intervals'], 'survey': survey}}
        )

#write_intervals(survey)
