from pymongo import MongoClient
from pprint import pprint
import datetime
import requests

question_types = ['Abierta', 'Numérica', 'Opción Múltiple', 'Rating']
client = MongoClient('mongodb+srv://SantiagoDZ:darmoslive@dzeeta-l6n4o.mongodb.net/test?retryWrites=true&w=majority')
db=client.live
user = '5d4bca2243bddbfe29078776'

'''
survey = {
    'user': '5d4bca2243bddbfe29078776',
    'created': datetime.datetime.now()
}

result=db.Survey.insert_one(survey)
pprint('Created {0}'.format(result.inserted_id))
'''

survey = '5d4edc1d3e0f3cf82b5159d2'
Questions = [
    {'title':'¿Qué número de partido es?', 'type':'Numérica', 'choices':[]},
    {'title':'¿What team played?', 'type':'Abierta', 'choices':[]},
    {'title':'¿Is it playing at home?', 'type':'Opción Múltiple', 'choices':['si', 'no']},
    {'title':'¿How many goals did they scored?', 'type':'Numérica', 'choices':[]},
    {'title':'¿What was the result?', 'type':'Abierta', 'choices':[]},
    {'title':'¿How many shots did you maked?', 'type':'Numérica', 'choices':[]},
    {'title':'¿How many shots were on target?', 'type':'Numérica', 'choices':[]},
    {'title':'¿How many fouls they committed?', 'type':'Numérica', 'choices':[]},
    {'title':'¿How many corner kicks there were?', 'type':'Numérica', 'choices':[]},
    {'title':'¿How many yellow cards they earned?', 'type':'Numérica', 'choices':[]},
    {'title':'¿Did they got red cards, how many?', 'type':'Numérica', 'choices':[]},
    {'title':'¿How many goals were at half time?', 'type':'Numérica', 'choices':[]},
    {'title':'¿What is the average bet for win?', 'type':'Numérica', 'choices':[]},
    {'title':'¿What is the average bet for draw?', 'type':'Numérica', 'choices':[]},
    {'title':'¿What are the bets for Over?', 'type':'Numérica', 'choices':[]}
]

'''
for i, q in enumerate(Questions):
    doc = db.Questions.insert_one({
        'id': i,
        'title': q['title'],
        'type': q['type'],
        'choices': q['choices'],
        'survey': survey,
        'created': datetime.datetime.now()
    })
    pprint('Created {0}'.format(doc.inserted_id))
'''

HOME_DICT = [
    'HomeTeam',
    'FTHG',
    'FTR',
    'HS',
    'HST',
    'HF',
    'HC',
    'HY',
    'HR',
    'HTHG',
    'B365H',
    'B365D',
    'BbMx>2.5',
]

AWAY_DICT = [
    'AwayTeam',
    'FTAG',
    'FTR',
    'AS',
    'AST',
    'AF',
    'AC',
    'AY',
    'AR',
    'HTAG',
    'B365A',
    'B365D',
    'BbMx>2.5'
]

Question_ids = [
    '5d4ef88f7bc7d8b9540f453b',
    '5d4ef8907bc7d8b9540f453d',
    '5d4ef8907bc7d8b9540f453e',
    '5d4ef8907bc7d8b9540f453f',
    '5d4ef8907bc7d8b9540f4540',
    '5d4ef8907bc7d8b9540f4541',
    '5d4ef8907bc7d8b9540f4542',
    '5d4ef8907bc7d8b9540f4543',
    '5d4ef8907bc7d8b9540f4544',
    '5d4ef8917bc7d8b9540f4545',
    '5d4ef8917bc7d8b9540f4546',
    '5d4ef8917bc7d8b9540f4547',
    '5d4ef8917bc7d8b9540f4548'
]

'''
url = 'https://pkgstore.datahub.io/sports-data/spanish-la-liga/season-1819_json/data/343671f6f60859a83fd6aae2a1a9dabb/season-1819_json.json'
resp = requests.get(url=url)
data = resp.json()
pprint(len(data))

for i, x in enumerate(data):
    h = []
    for i1, y in enumerate(HOME_DICT):
        h.append({'question':Question_ids[i1], 'value':x[y], 'survey':survey, 'user': i*2, 'date': datetime.datetime.strptime(x['Date'], '%d/%m/%Y')})
    h.append({'question':'5d4ef88d7bc7d8b9540f453a', 'value':i, 'survey':survey, 'user': i*2, 'date': datetime.datetime.strptime(x['Date'], '%d/%m/%Y')})
    h.append({'question':'5d4ef8907bc7d8b9540f453c', 'value':'si', 'survey':survey, 'user': i*2, 'date': datetime.datetime.strptime(x['Date'], '%d/%m/%Y')})
    db.Answers.insert_many(h)
    print('Home:')
    a = []
    for i2, z in enumerate(AWAY_DICT):
        a.append({'question':Question_ids[i2], 'value':x[z], 'survey':survey, 'user': i*2 + 1, 'date': datetime.datetime.strptime(x['Date'], '%d/%m/%Y')})
    a.append({'question':'5d4ef88d7bc7d8b9540f453a', 'value':i, 'survey':survey, 'user': i*2 + 1, 'date': datetime.datetime.strptime(x['Date'], '%d/%m/%Y')})
    a.append({'question':'5d4ef8907bc7d8b9540f453c', 'value':'no', 'survey':survey, 'user': i*2 + 1, 'date': datetime.datetime.strptime(x['Date'], '%d/%m/%Y')})
    db.Answers.insert_many(a)
    print('Away:')

print(len(Questions))
print(len(HOME_DICT))
print(len(AWAY_DICT))
'''
