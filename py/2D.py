from pymongo import MongoClient
from itertools import product

client = MongoClient('mongodb+srv://SantiagoDZ:darmoslive@dzeeta-l6n4o.mongodb.net/test?retryWrites=true&w=majority')
db = client.live
survey = '5d4edc1d3e0f3cf82b5159d2'

def get_numeric_questions(survey):
    return list(db.Questions.find({'survey':survey, 'type':'Númerica'}))

def calculate_correlation(question1, question2):
    correlation_pipeline = [
        {'$match': {'$or': [{'question': question1}, {'question': question2}]}}, 
        {'$group': {
            '_id': '$question', 
            'mean': {'$avg': '$value'}, 
            'stdDev': {'$stdDevPop': '$value'}, 
            'values': {'$push': {'user': '$user', 'value': '$value'}}
        }}, 
        {'$project': {'mean': '$mean', 'stdDev': '$stdDev', 'values': {'$map': {'input': '$values', 'as': 'v', 'in': {
            'user': '$$v.user', 
            'value': {'$divide': [{'$subtract': ['$$v.value', '$mean']}, '$stdDev']}
        }}}}}, 
        {'$unwind': '$values'}, 
        {'$project': {'value': '$values.value', 'user': '$values.user'}}, 
        {'$group': {'_id': '$user', 'value': {'$push': '$value'}}}, 
        {'$sort': {'_id': 1}}, 
        {'$project': {'correlation': {'$reduce': {'input': '$value', 'initialValue': 1, 'in': {
            '$multiply': ['$$value', '$$this']
        }}}}}, 
        {'$group': {'_id': 'correlation', 'correlation': {'$sum': '$correlation'}, 'count': {'$sum': 1}}}, 
        {'$project': {'coefficient': {'$divide': ['$correlation', {'$subtract': ['$count', 1]}]}}}
    ]
    return list(db.Answers.aggregate(correlation_pipeline))[0]['coefficient']

def create_map_of_answers(survey):
    question_dict = {}
    questions = get_numeric_questions(survey)
    question_ids = list(map(lambda doc: str(doc['_id']), questions))
    for question in questions: question_dict[str(question['_id'])] = question['title']
    question_pairs = set(product(question_ids, question_ids))
    question_permutations = set((a,b) if a<b else (b,a) for a,b in question_pairs)
    answers_map = list(filter(lambda a: a[0] != a[1], question_permutations))
    return answers_map, question_dict

def generate_correlations(survey):
    answers_map, question_dict = create_map_of_answers(survey)
    correlations = []
    for answer in answers_map:
        correlation = calculate_correlation(answer[0], answer[1])
        correlation_document = {
            'survey': survey,
            'question_one_id': answer[0],
            'question_two_id': answer[1],
            'question_one_title': question_dict[answer[0]],
            'question_two_title': question_dict[answer[1]],
            'correlation': correlation
        }
        correlations.append(correlation_document)
    return correlations

def write_correlation(survey): db.Metrics.insert_many(generate_correlations(survey))
write_correlation(survey)
