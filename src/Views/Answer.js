import { Anchor, Box, Button, Heading, Grommet, InfiniteScroll, Menu, Text } from 'grommet'
import { AnonymousCredential } from 'mongodb-stitch-browser-sdk'
import { AppBar, Intro, Outro } from '../Widgets/Layout'
import React, { Component, Fragment } from 'react'
import { Answer } from '../Widgets/Survey'
import { client, db } from '../Config/db'
import { CaretDown } from 'grommet-icons'
import { grommet } from 'grommet/themes'
import { Link } from 'react-router-dom'

export class Response extends Component {
    state = { 
      user: null,
      survey: null,
      started: false,
      answered: false,
      user_surveys: [],
      created_questions: [] 
    }
  
    componentDidMount(){
        client.auth.loginWithCredential(new AnonymousCredential())
        .then(() => db.collection('rh').updateOne({owner_id: client.auth.user.id}, {$set:{number:42}}, {upsert:true}))
        .then(() => db.collection('Questions').find({survey: this.props.match.params.id}).asArray())
        .then(docs => this.setState({user: client.auth.user.id, survey: this.props.match.params.id, created_questions: 
            docs.map((d, i) => ({id:d._id.toString(), title:d.title, type:d.type, choices:d.choices, value:undefined }))
        }, () => console.log(this.state)))
        .then(() => db.collection('rh').find({owner_id: client.auth.user.id}, { limit: 100}).asArray())
        .then(() => db.collection('Survey').find({user: client.auth.user.id}).asArray())
        .then(docs => this.setState({user_surveys:docs}))
        .catch(console.log)
    }
    
    set_answer = (val, i) => this.setState({created_questions: 
        this.state.created_questions.map(q => q.id === i ? {...q, value:val, created: new Date()} : q)
    })

    next_answer = (i) => document.getElementById(`answer-${i}`).click()
    save_answers = () => db.collection('Answers').insertMany(
        this.state.created_questions.map(q => ({
            survey: this.state.survey, question:q.id, value: q.value, user: this.state.user, created: q.created
        }))
    )
    .then(() => this.setState({ answered: true })).catch(console.log)

    render() {
      const { created_questions, user_surveys, started, answered } = this.state
      const { next_answer, set_answer, save_answers } = this
      return (
        <Grommet theme={grommet} full>
          <AppBar>
            <Heading level='3' margin='none' style={{ fontSize:33 }}>Darmos Live</Heading>
            <Menu
              label={<Text>Mis Evaluaciones</Text>}
              items={user_surveys.reverse().map((s,i) => ({label: 
                <Link to={`/dashboard/${s._id.toString()}`} style={{textDecoration:'none'}}>
                  {`Evaluación ${user_surveys.length - i}`}
                </Link>
              }))}
            />
          </AppBar>
          <Intro hidden={started} onClick = {() => this.setState({started:true})}/>
          { started && !answered 
          ? <Box direction='row' overflow={{ horizontal: 'hidden' }} style = {{minHeight:'100vh'}} background='linear-gradient(0.5turn, #FFFFFF, #E2E2E2)'>
                <Box flex align='center' justify='center'> 
                    <Anchor name='start'/> 
                    <Heading level='3' color='#7D4CDB' style = {{marginTop:'5vh'}}>Evaluación</Heading>
                    <InfiniteScroll items={created_questions}>
                        {(item, i) => <Fragment key={i} >
                            <Anchor id={`answer-${i}`} href={`#a-${i}`}/>
                            <Answer 
                                type={item.type} title={`${i+1}. ${item.title}`} choices={item.choices} minHeight={1}
                                margin={'25vh'} val={item.value} setVal={set_answer} id = {item.id}
                            />
                            { (i + 1) !== created_questions.length 
                                ? <CaretDown onClick = {() => next_answer(i)} style={{cursor:'pointer'}}/>
                                : <Button label={'Enviar'} onClick = {() => save_answers()} style={{margin:25, marginBottom:'50vh'}}/>
                            }
                            <Anchor name={`a-${i}`}/>
                        </Fragment> } 
                    </InfiniteScroll>
                </Box>
            </Box>
          : null }
          <Outro hidden={!answered} url={''}/>
        </Grommet>
      )
    }
  }
  