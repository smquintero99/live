import { Anchor, Box, Heading, Grommet, Menu, Text, Chart, Stack, Select } from 'grommet'
import { Scatter, scatter_pipeline, HeatMap } from '../Components/Charts'
import { AnonymousCredential } from 'mongodb-stitch-browser-sdk'
import { AppBar } from '../Widgets/Layout'
import { client, db } from '../Config/db'
import React, { Component } from 'react'
import { grommet } from 'grommet/themes'
import { Link } from 'react-router-dom'

const metric_background = {color: 'light-4', opacity: 'strong'}
const YLabel = ({value}) => <Box width='xxsmall'>
  <Box flex justify='between'>
    <Box align='end'>
      <Box pad='xsmall' background={{ color: 'white', opacity: 'medium' }}>
        <Text>{value}</Text>
      </Box>
    </Box>
  </Box>
  <Box height='xxsmall' flex={false}/>
</Box>

class PrettyChart extends Component{
  state = {active: null}
  valueRefs = []
  render(){
    const { active } = this.state
    const { data, legend, select_choices, set_select_value } = this.props
    const max_y_value = data.length > 0 ? Math.max(...data.map(d => d.value[1])) : 0
    const min_x_value = data.length > 0 ? Math.min(...data.map(d => Number(d.label))) : 0
    const max_x_value = data.length > 0 ? Math.max(...data.map(d => Number(d.label))) : 0
    return(
      <Box tabIndex='0' direction='row' margin='large'>
        <YLabel value={  max_y_value }/>
        <Box width='large'>
          <Stack guidingChild='first'>
            <Chart values={ data } aria-label='chart' size={{ width: 'full', height: 'small' }}
               bounds={[[0, data.length - 1], [0, max_y_value]] }/>
            <Box fill direction='row' justify='between'>
              {data.reverse().map((v, i) => (<Box flex={false} key={i}>
                <Stack fill anchor='center' interactiveChild='first'>
                  <Box fill pad={'small'} ref={ref => {this.valueRefs[i] = ref}}
                    background={active === i ? { color: 'dark-5', opacity: 'medium' } : undefined}
                    onMouseOver={() => this.setState({ active: i })}
                    onMouseOut={() =>this.setState({ active: undefined })}
                  />{ active === i && (<Box animation={{ type: 'fadeIn', duration: 100}} 
                      width='small' pad='small' round='small' background='dark-3'
                    >
                      <Text size='large'>{`Frequency: ${v.value[1]}`}</Text>
                      <Text size='small'>{`Minimum Value: ${v.label}`}</Text>
                    </Box>)}
                </Stack>
              </Box>))}
            </Box>
          </Stack>
          <Box height='xxsmall' direction='row' justify='between' align='center'  border='top' style={{paddingTop:20}}>
            {[min_x_value, legend, max_x_value].map((t, i) => typeof(t) === 'string'
              ? <Select key={i} options={select_choices} value={t} onChange={({ option }) => set_select_value(option)}/> 
              : <Text key={i} style={{width:'20%', textAlign: i === 0 ? 'left' : 'right'}}>{t}</Text> )}
          </Box>
        </Box>
      </Box>      
    )
  }
}

export class Dashboard extends Component {
  state = { user: null, survey: null, user_surveys: [], created_questions: [], metrics:[], graph_data:[], graph_legend:'',
    graph_select_choices: [], scatter_data:[], scatter_selects:{value_one:'', value_two:''}, 
    heat_map:{value_heat_one:'', value_heat_two:'', choices:[], heat_data:[]}}
  componentDidMount(){
    client.auth.loginWithCredential(new AnonymousCredential())
    .then(() => db.collection('rh').updateOne({owner_id: client.auth.user.id}, {$set:{number:42}}, {upsert:true}))
    .then(() => db.collection('Survey').find({user: client.auth.user.id}).asArray())
    .then(docs => this.setState({user_surveys:docs.reverse().map(survey => ({...survey, id:survey._id.toString()}))}))
    .then(() => db.collection('Questions').find({survey: this.props.match.params.id}).asArray())
    .then(docs => this.setState({user: client.auth.user.id, survey: this.props.match.params.id, created_questions: 
        docs.map(d => ({id:d._id.toString(), title:d.title, type:d.type, choices:d.choices, value:undefined }))}, 
        () => this.retrieve_scatter()))
    .then(() => db.collection('Metrics').find({survey: this.props.match.params.id}).asArray())
    .then((docs) => this.setState({metrics: docs.map(d=> ({...d, question_id: d.question_id ? d.question_id.toString(): null})), 
      graph_legend: docs.find(m => m.intervals).title,
      graph_select_choices: docs.filter(m => m.intervals).map(m => m.title),
      graph_data: this.set_data_value(docs)}, () => this.retrive_heat()))
    .then(() => db.collection('rh').find({owner_id: client.auth.user.id}, { limit: 100}).asArray())    
    .catch(console.log)
  }
  
  set_select_value = v => this.setState({graph_legend: v, graph_data: this.set_data_value(this.state.metrics, v)})
  set_data_value = (docs, title) => docs.find(m => title ? m.intervals && m.title === title : m.intervals).intervals
    .sort((a,b) => a.group > b.group ? 1 : -1).map((i, j) => ({value:[j, i.frecuency], label:String(i.group) }))
  retrieve_scatter = (value_one, value_two) => {
    const questions = this.state.created_questions.filter(q => q.type === 'Númerica')
    const question_one = value_one ? questions.find(q => q.title === value_one) : questions[0]
    const question_two = value_two ? questions.find(q => q.title === value_two) : questions[1]
    db.collection('Answers').aggregate(scatter_pipeline(question_one.id, question_two.id)).toArray()
    .then(docs => this.setState({scatter_data:docs, 
      scatter_selects:{value_one: question_one.title, value_two: question_two.title}}))}
  retrive_heat = async(value_one, value_two) => {
    const questions = this.state.metrics.filter(m => m.intervals && m.intervals.length > 1 && m.intervals.length < 20)
    const question_one = value_one ? questions.find(q => q.title === value_one) : questions[2]
    const question_two = value_two ? questions.find(q => q.title === value_two) : questions[3]
    const data = await db.collection('Answers').aggregate(
      scatter_pipeline(question_one.question_id, question_two.question_id)).toArray()
    this.setState({heat_map:{choices: questions.map(m => m.title), value_heat_one: question_one.title, 
      value_heat_two: question_two.title, heat_data: data.map(d => ({...d, color:d.size})) }}, () => console.log(this.state))
  }
  render() {
    const { user_surveys, survey, metrics, graph_data, graph_legend, graph_select_choices, scatter_data } = this.state
    const { scatter_selects, created_questions, heat_map } = this.state
    const { choices, value_heat_one, value_heat_two, heat_data } = heat_map
    const { value_one, value_two } = scatter_selects
    const { set_select_value, retrieve_scatter, retrive_heat } = this
    return (
      <Grommet theme={grommet} full>
        <AppBar>
          <Heading level='3' margin='none' style={{ fontSize:33 }}>Darmos Live</Heading>
          <Menu label={<Text>Mis Evaluaciones</Text>}
            items={user_surveys.map((s,i) => ({label:
              <Link to={`/dashboard/${s._id.toString()}`} style={{textDecoration:'none'}}>
                {`Evaluación ${user_surveys.length - i}`}
              </Link>
            }))}
          />
        </AppBar>
        <Box direction='row' overflow={{ horizontal: 'hidden', vertical: 'hidden' }} 
          style = {{minHeight:'100vh', paddingBottom:'20vh'}} background='linear-gradient(0.5turn, #FFFFFF, #E2E2E2)'
        >
          <Box flex align='center' justify='center'> 
            <Heading level='3' color='#7D4CDB' style = {{marginTop:'5vh'}}>
              Evaluación { user_surveys.reverse().findIndex(s => s.id === survey) + 1}
            </Heading>          
            <Box direction='row' gap='medium' overflow={{ horizontal: 'auto' }} style={{width:'90%', paddingBottom:10}}>
              {metrics.filter(metric => metric.avg).map((metric, idx) => 
                <Box pad='medium' align='center' background={metric_background} round={'small'} 
                  gap='small' key={idx} style={{minWidth:180}}
                >
                  <Text color='#505050'>{ metric.title }</Text>
                  <Anchor label={ Math.round(metric.avg * 100)/100 } size = 'xlarge' style = {{marginTop:'auto'}}/>
                </Box>
              )}
            </Box>
            <Heading level='3' color='#7D4CDB' style = {{marginTop:'5vh'}}> Cummulative Graph </Heading>
            <PrettyChart data = { graph_data } legend = { graph_legend } select_choices = { graph_select_choices } 
              set_select_value = { set_select_value }/>
            <Box flex align='center' justify='center'>
              <Heading level='3' color='#7D4CDB' style = {{marginTop:'5vh'}}>Scatter</Heading>
              <Scatter data={scatter_data}/>
              <Box height='xxsmall' direction='row' justify='between' align='center' border='top' style={{paddingTop:20}}>
                <Select options={ created_questions.filter(q => q.type === 'Númerica' ).map(q => q.title) } margin={'small'}
                  value = { value_one || '' } onChange = {({option}) => retrieve_scatter(option, value_two)}/>
                <Select options={ created_questions.filter(q => q.type === 'Númerica' && q.title !== value_one ).map(q => q.title) } 
                  margin={'small'} value = { value_two || '' } onChange = {({option}) => retrieve_scatter(value_one, option)}/>
              </Box>
              <Heading level='3' color='#7D4CDB' style = {{marginTop:'5vh'}}>HeatMap</Heading>
              <HeatMap data={ heat_data }/>
              <Box height='xxsmall' direction='row' justify='between' align='center' border='top' style={{paddingTop:20}}>
                <Select options={ choices } margin={'small'} value = { value_heat_one || '' } 
                  onChange = {({option}) => retrive_heat(option, value_two)}/>
                <Select options={ choices } margin={'small'} value = { value_heat_two || '' } 
                  onChange = {({option}) => retrive_heat(value_one, option)}/>
              </Box>
            </Box>
          </Box>
        </Box>
      </Grommet>
    )
  }
}
