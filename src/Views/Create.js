import { Anchor, Box, Button, Heading, Grommet, InfiniteScroll, Menu, Text } from 'grommet'
import { AnonymousCredential } from 'mongodb-stitch-browser-sdk'
import { AppBar, Intro, Outro } from '../Widgets/Layout'
import { Answer, Question } from '../Widgets/Survey'
import { client, db } from '../Config/db'
import React, { Component } from 'react'
import { grommet } from 'grommet/themes'
import { Link } from 'react-router-dom'

const root = `http://localhost:3000/`
export class Create extends Component {
    state = { 
      user: null,
      user_surveys: [],
      survey_url: null,
      created_questions: [{id:0, title:'', type:'', choices:[]}],
      started: false,
      published: false
    }
  
    componentDidMount(){
      client.auth.loginWithCredential(new AnonymousCredential())
      .then(() => db.collection('rh').updateOne({owner_id: client.auth.user.id}, {$set:{number:42}}, {upsert:true}))
      .then(() => db.collection('rh').find({owner_id: client.auth.user.id}, { limit: 100}).asArray())
      .then(() => this.setState({user: client.auth.user.id}))
      .then(() => db.collection('Survey').find({user: client.auth.user.id}).asArray())
      .then(docs => this.setState({user_surveys:docs}))
      .catch(console.log)
    }
  
    set_title = (e, id) => this.setState({created_questions: 
      this.state.created_questions.map(q => q.id === id ? {...q, title:e, created: new Date()} : q)
    })
    set_type = (e, id) => this.setState({created_questions:
      this.state.created_questions.map(q => q.id === id ? {...q, type:e, created: new Date()} : q)
    })
    add_choice = (e, id) => this.setState({created_questions:
      this.state.created_questions.map(q => q.id === id ? {...q, choices:[...q.choices, e], created: new Date()} : q)
    })
    remove_choice = (e, id) => this.setState({created_questions:
      this.state.created_questions.map(q => q.id === id ? {...q, choices:q.choices.filter(c => c !== e), created: new Date()} : q)
    })
    add_question = () => this.setState({created_questions:[
      ...this.state.created_questions, 
      {id: this.state.created_questions.length, title:'', type:'', choices:[]}
    ]}, () => setTimeout(()=> document.getElementById(`question-${this.state.created_questions.length - 1}`)
      ? document.getElementById(`question-${this.state.created_questions.length - 1}`).click() 
      : console.log(`question-${this.state.created_questions.length - 1}`),100))
    save_survey = () => db.collection('Survey').insertOne({user: this.state.user, created: new Date()})
      .then(({insertedId}) => this.setState({published: true, survey_url: `${root}${insertedId}`},
        () => db.collection('Questions').insertMany(
          this.state.created_questions.map(q => ({ ...q, survey: insertedId.toString()}))
        ))
      )
      .catch(console.log)
  
    render() {
      const { created_questions, user_surveys, started, published, survey_url } = this.state
      const { set_title, set_type, add_choice, remove_choice, add_question, save_survey } = this
      return (
        <Grommet theme={grommet} full>
          <AppBar>
            <Heading level='3' margin='none' style={{ fontSize:33 }}>Darmos Live</Heading>
            <Menu
              label={<Text>Mis Evaluaciones</Text>}
              items={user_surveys.reverse().map((s,i) => ({label: 
                <Link to={`/dashboard/${s._id.toString()}`} style={{textDecoration:'none'}}>
                  {`Evaluación ${user_surveys.length - i}`}
                </Link>
              }))}
            />
          </AppBar>
          <Intro hidden={started} onClick = {() => this.setState({started:true})}/>
          { started && !published 
          ? <Box direction='row' overflow={{ horizontal: 'hidden' }} style = {{minHeight:'100vh'}}>
              <Box flex align='center' justify='center'> 
                <Anchor name='start'/> 
                <Heading level='3' color='#7D4CDB' style = {{marginTop:'10vh'}}>Campo de Edición</Heading>
                <InfiniteScroll items={created_questions}>
                  {(item) => <Question 
                    id={item.id} 
                    key = {item.id}
                    title={item.title}
                    type={item.type}
                    set_title={set_title} 
                    set_type={set_type}
                    choices={item.choices}
                    add_choice={add_choice}
                    remove_choice={remove_choice}
                  /> }
                </InfiniteScroll>
                <Button label={'Nueva Pregunta'} onClick={add_question} style={{margin:25, marginBottom:'10vh'}}/>
              </Box>
              <Box width='50vw' background='light-2' elevation='small' align='center' justify='center'> 
                <Heading level='3' color='#7D4CDB' style={{marginTop:'10vh'}}>Vista Previa</Heading>
                <InfiniteScroll items={created_questions}>
                  {(item) => <Answer key={item.id} type={item.type} title={item.title} choices={item.choices}/> }                      
                </InfiniteScroll>
                <Button label='Publicar Evaluación' onClick={() => save_survey()} style={{margin:25, marginBottom:'10vh'}}/>
              </Box>
            </Box>
          : null }
          <Outro hidden={!published} url={survey_url}/>
        </Grommet>
      )
    }
  }
  