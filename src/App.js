import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Dashboard } from './Views/Dashboard'
import { Response } from './Views/Answer'
import { Create } from './Views/Create'
import React from 'react'
import './App.css'

const App = () => <BrowserRouter>
  <Switch>
    <Route exact path='/' name='Crear Evaluación' component={ Create } />
    <Route exact path='/:id' name='Contestar Evaluación' component={ Response } />
    <Route exact path='/crear' name='Crear Evaluación' component={ Create } />
    <Route exact path='/dashboard/:id' name='Dashboard' component={ Dashboard } />
  </Switch>
</BrowserRouter>

export default App
