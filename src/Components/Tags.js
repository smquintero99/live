import { Box, Button, Keyboard, Text, TextInput } from 'grommet'
import React, { createRef, Component } from 'react'
import { FormClose } from 'grommet-icons'

const Tag = ({ children, onRemove, ...rest }) => {
  const tag = (
    <Box
      direction='row'
      align='center'
      background='brand'
      pad={{ horizontal: 'xsmall', vertical: 'xxsmall' }}
      margin={{ vertical: 'xxsmall' }}
      round='medium'
      {...rest}
    >
      <Text size='xsmall' margin={{ right: 'xxsmall' }}>
        {children}
      </Text>
      {onRemove && <FormClose size='small' color='white' />}
    </Box>
  )
  return onRemove
  ? <Button onClick={onRemove}>{tag}</Button>
  : tag
}

class TagInput extends Component {
  state = { currentTag: '' }
  boxRef = createRef()
  componentDidMount() { this.forceUpdate() }
  updateCurrentTag = event => {
    const { onChange } = this.props
    this.setState({ currentTag: event.target.value })
    if (onChange) { onChange(event) }
  }

  onAddTag = tag => {
    const { onAdd } = this.props
    if (onAdd) { onAdd(tag) }
  }

  onEnter = () => {
    const { currentTag } = this.state
    if (currentTag.length) {
      this.onAddTag(currentTag)
      this.setState({ currentTag: '' })
    }
  }

  renderValue = () => {
    const { value, onRemove } = this.props
    return value.map((v, index) => (
      <Tag margin='xxsmall' key={`${v}${index}`} onRemove={() => onRemove(v)}> {v} </Tag>
    ))
  }

  render() {
    const { value = [], onAdd, onRemove, onChange, ...rest } = this.props
    const { currentTag } = this.state
    return (
      <Keyboard onEnter={this.onEnter}>
        <Box
          direction='row'
          align='center'
          pad={{ horizontal: 'xsmall' }}
          border='all'
          ref={this.boxRef}
          wrap
        >
          {value.length > 0 && this.renderValue()}
          <Box flex style={{ minWidth: '120px' }}>
            <TextInput
              type='search'
              plain
              dropTarget={this.boxRef.current}
              {...rest}
              onChange={this.updateCurrentTag}
              value={currentTag}
              onSelect={event => {
                event.stopPropagation()
                this.onAddTag(event.suggestion)
              }}
            />
          </Box>
        </Box>
      </Keyboard>
    )
  }
}

export class Tags extends Component {
  state = {
    selectedTags: [],
  }

  onRemoveTag = tag => {
    const { selectedTags } = this.state
    const removeIndex = selectedTags.indexOf(tag)
    const newTags = [...selectedTags]
    if (removeIndex >= 0) { newTags.splice(removeIndex, 1) }
    this.setState({ selectedTags: newTags })
  }

  onAddTag = tag => {
    const { selectedTags } = this.state
    this.setState({ selectedTags: [...selectedTags, tag] })
  }

  render() {
    const { id, choices, add_choice, remove_choice } = this.props
    return (
        <TagInput
            value={choices}
            onAdd={(tag) => add_choice(tag, id)}
            onRemove={(tag) => remove_choice(tag, id)}
            placeholder='Seleccionar Opciones'
        />
    )
  }
}
