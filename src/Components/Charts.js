import { MarkSeries, XYPlot, HeatmapSeries } from 'react-vis'
import React from 'react'


export const Scatter = ({data}) => <XYPlot width={300} height={300}>
    <MarkSeries className="mark-series-example" sizeRange={[5, 15]} data={data}/>
</XYPlot>

export const HeatMap = ({data}) => {
return <XYPlot width={300} height={300}>
    <HeatmapSeries className="heatmap-series-example" colorRange={["#6464E1", '#EC3326']} data={data}/>
</XYPlot>
}

export const scatter_pipeline = (question_one, question_two) => [
    {'$match': {'$or': [{'question': question_one}, {'question': question_two}]}}, 
    {'$group': {'_id': '$user', 'values': {'$push': {'$cond': {
        'if': {'$eq': ['$question', question_one]}, 'then': {'x': '$value'}, 'else': {'y': '$value'}
    }}}}}, 
    {'$project': {'values': {'$mergeObjects': '$values'}}}, 
    {'$group': {'_id': {'x': '$values.x', 'y': '$values.y'}, 'size': {'$sum': 1}}}, 
    {'$project': {'_id': 0,'x': '$_id.x', 'y': '$_id.y', 'size': '$size'}}
]
