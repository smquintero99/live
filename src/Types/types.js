export const Company = {
    name: 'string',
    user: 'ObjectID',
    created: 'date'
}

export const Survey = {
    user: 'ObjectID',
    created: 'date',
    name: 'string'
}

export const Questions = {
    survey: 'ObjectId',
    title: 'string',
    type: 'enum/string',
    id: 'number',
    choices: 'array - strings',
    created: 'date'
}

export const Answers = {
    question: 'ObjectId',
    value: 'string',
    created: 'date',
    user: 'ObjectId'
}
