import { Stitch, RemoteMongoClient } from 'mongodb-stitch-browser-sdk'

export const client = Stitch.initializeDefaultAppClient('live-xosht')
export const db = client.getServiceClient(RemoteMongoClient.factory, 'live-db').db('live')
