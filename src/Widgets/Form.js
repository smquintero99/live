import { Box, TextInput, Select, TextArea, Stack, Text, RangeInput, Grommet } from 'grommet'
import { grommet } from 'grommet/themes'
import { deepMerge } from 'grommet/utils'
import React from 'react'

const placeholder = 'Ingrese la respuesta'
export const Open = ({id, val, setVal}) => {
  const [value, setValue] = React.useState('')
  return (
    <TextArea
      placeholder={placeholder}
      value={setVal ? val : value}
      onChange={event => setVal ? setVal(event.target.value, id) : setValue(event.target.value)}
    />
  )
}

export const Numeric = ({id, val, setVal}) => {
  const [value, setValue] = React.useState('')
  return(
    <TextInput
      type = 'number'
      placeholder={placeholder}
      value={setVal ? val || '' : value}
      onChange={event => setVal ? setVal(event.target.value, id) : setValue(event.target.value)}
    />
  )
}

export const Options = ({id, val, setVal, choices}) => {
  const [value, setValue] = React.useState('')
  return (
    <Select options={choices} placeholder={placeholder} value={setVal ? val || '' : value}
      onChange={({ option }) => setVal ? setVal(option, id) : setValue(option)}
    />
  )
}

export const Rating = ({id, val, setVal}) => {
  const [value, setValue] = React.useState('')
  return (
    <Grommet theme={customThemeRangeInput}>
      <Stack>
        <Box direction='row' justify='between' style={{backgroundColor:'rgba(255,255,255,0)'}}>
          {[0, 1, 2, 3, 4, 5].map(value => (
            <Box key={value} pad='small' border={false} style={{marginTop:15, backgroundColor:'rgba(255,255,255,0)'}}>
              <Text style={{ fontFamily: 'monospace' }}>
                {value}
              </Text>
            </Box>
          ))}
        </Box>
        <RangeInput style = {{paddingRight:10, paddingLeft:6}} direction='horizontal' 
          invert={false} min={0} max={5} size='full' round='small' value={setVal ? val || '' : value}
          onChange={event => setVal ? setVal(event.target.value, id) : setValue(event.target.value)}
        />
      </Stack>
    </Grommet>
    )
}

const customThemeRangeInput = deepMerge(grommet, {
  global: { spacing: '12px' },
  rangeInput: {
    track: {
      color: 'accent-2',
      height: '12px',
      extend: () => `border-radius: 10px`
    },
    thumb: { color: 'neutral-2' }
  }
})
