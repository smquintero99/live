import { Open, Numeric, Options, Rating } from '../Widgets/Form'
import { Box, TextInput, Select, Text, Anchor } from 'grommet'
import { Tags } from '../Components/Tags'
import React from 'react'

const question_types = ['Abierta', 'Númerica', 'Opción Múltiple', 'Rating']
export const InputQuestion = ({id, title, set_title}) => <TextInput
    placeholder={`Añadir Pregunta ${id+1}`}
    value={title}
    onChange={event => set_title(event.target.value, id)}
/>
  
const SelectType = ({id, type, set_type}) => <Select
    placeholder = 'Tipo de Pregunta'
    options={ question_types }
    value={ type }
    onChange={({ option }) => set_type(option, id)}
/>
  
const question_background = { color: 'light-2', opacity: 'strong' }
export const Question = ({id, title, type, set_title, set_type, choices, add_choice, remove_choice}) => <Box 
    pad='large' align='center' background={question_background} round gap='small' width='80%' style={{margin:'20vh'}}
>
    <Anchor name={`q-${id}`}/>
    <Anchor id={`question-${id}`} href={`#q-${id}`}/>
    <InputQuestion id={id} title={title} set_title={set_title}/>
    <SelectType id={id} type={type} set_type={set_type}/>
    { type === 'Opción Múltiple' 
        ? <Tags id={id} choices={choices} add_choice={add_choice} remove_choice={remove_choice}/> 
        : null
    }
</Box>

const answer_background = { color: 'white', opacity: 'strong' }
export const Answer = ({ id, title, type, val, setVal, choices, minHeight, margin }) => <Box 
    pad='large' align='center' round gap='small' width='80%' background={answer_background} 
    style={{margin:margin || '20vh', minHeight:minHeight || '228px', maxWidth:650 }}
>
    <Text style={{marginRight:'auto'}}>{title}</Text>
    { get_children(id, type, val, setVal, choices) }
</Box>


const get_children = (id, type, val, setVal, choices) => ({
    'Abierta': <Open id={id} val={val} setVal={setVal}/>,
    'Númerica': <Numeric id={id} val={val} setVal={setVal}/>, 
    'Opción Múltiple': <Options id={id} choices={choices} val={val} setVal={setVal}/>, 
    'Rating': <Rating id={id} val={val} setVal={setVal}/>
}[type || 'Abierta'])
