import { FacebookShareButton, LinkedinShareButton, TwitterShareButton, WhatsappShareButton, EmailShareButton } from 'react-share'
import { Anchor, Box, Heading, Paragraph } from 'grommet'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { SocialIcon } from 'react-social-icons'
import React from 'react'

export const AppBar = (props) => (
    <Box
      tag='header'
      direction='row'
      align='center'
      justify='between'
      background='brand'
      pad={{ left: 'medium', right: 'small', vertical: 'small' }}
      elevation='medium'
      style={{ zIndex: '1' }}
      {...props}
    />
)

export const Intro = ({hidden, onClick}) => !hidden
? <Box
    height= '100vh'
    justify='center'
    align='center'
    pad='xlarge'
    background='linear-gradient(0.25turn, #FFFFFF, #E2E2E2)'
    round='false'
>
    <Heading color='#7D4CDB'>¡Bienvenido!</Heading>
    <Paragraph color='#444444' style = {{textAlign:'center', maxWidth:550}}> 
      Ingresa a continuación los compartamientos que desees <b>evaluar</b>, 
      cuando hayas terminado <b>publica</b> la evaluación.
    </Paragraph>
    <Anchor id='anchor' href='#start' primary label='Iniciar' style={{margin:20}} onClick={onClick}/>
</Box>
: null

export const Outro = ({hidden, url}) => !hidden
? <Box
    height= '100vh'
    justify='center'
    align='center'
    pad='xlarge'
    background='linear-gradient(0.25turn, #FFFFFF, #E2E2E2)'
    round='false'
>
    <Heading color='#7D4CDB'>¡Buen Trabajo!</Heading>
    <CopyToClipboard text={url}>
      <Paragraph color='#444444' style = {{textAlign:'center', maxWidth:550}}> 
        Tu evaluación está lista para ser compartida. Haz 
        <Anchor primary label=' click ' style={{margin:0}}/> 
        para copiar la dirección o compártela en tu red social favorita.
      </Paragraph>
    </CopyToClipboard>
    <div style={{display:'inline-flex'}}>
      <FacebookShareButton url={url} style={{margin:20}}><SocialIcon url='https://facebook.com'/></FacebookShareButton>
      <TwitterShareButton url={url} style={{margin:20}}><SocialIcon url='https://twitter.com' /></TwitterShareButton>
      <WhatsappShareButton url={url} style={{margin:20}}><SocialIcon url='https://web.whatsapp.com/' /></WhatsappShareButton>
      <LinkedinShareButton url={url} style={{margin:20}}><SocialIcon url='https://linkedin.com' /></LinkedinShareButton>
      <EmailShareButton url={url} style={{margin:20}}><SocialIcon url='https://email.com' /></EmailShareButton>
    </div>
</Box>
: null
